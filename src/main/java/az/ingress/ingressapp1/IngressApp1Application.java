package az.ingress.ingressapp1;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class IngressApp1Application {

    public static void main(String[] args) {
        SpringApplication.run(IngressApp1Application.class, args);
    }

}
