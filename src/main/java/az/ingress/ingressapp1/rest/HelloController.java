package az.ingress.ingressapp1.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping(value = "/ingress/sayHello")
public class HelloController {

    @GetMapping
    public String sayHello() {
        return "Hello, Ingress";
    }

}
